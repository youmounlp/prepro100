[The-Japan-DataScientist-Society/100knocks-preprocess: データサイエンス100本ノック（構造化データ加工編）](https://github.com/The-Japan-DataScientist-Society/100knocks-preprocess)をシェルスクリプトで解く遊びです。  
  
## 環境構築
UNIXコマンドのみを使うためデータのみDLします。  
  
### ダウンロード
データは
[100knocks-preprocess/docker/work/data at master · The-Japan-DataScientist-Society/100knocks-preprocess](https://github.com/The-Japan-DataScientist-Society/100knocks-preprocess/tree/master/docker/work/data)
にあるのでsvnで取得します。  
  
```
svn export https://github.com/The-Japan-DataScientist-Society/100knocks-preprocess/trunk/docker/work/data
```
  

### 改行コードについて
```
find . -name \*.csv | xargs file
./product.csv:  UTF-8 Unicode (with BOM) text, with CRLF line terminators
./customer.csv: UTF-8 Unicode text
./store.csv:    UTF-8 Unicode text
./category.csv: UTF-8 Unicode (with BOM) text, with CRLF line terminators
./receipt.csv:  ASCII text
./geocode.csv:  UTF-8 Unicode (with BOM) text, with CRLF line terminators
```
  
私の環境のせいかもしれませんがCRLF改行を含むファイルがありました。  
  
CRLFのままだとjoinなど一部のコマンドが予期せぬ動作をするのでnkfでLF改行に変換しておきます。  
**！ファイルが上書きされます！**  
  
```
find . -name \*.csv | xargs nkf -Lu --overwrite
```