#1
head receipt.csv -n11

#2
head receipt.csv -n11 | awk -F, '{print $1,$6,$7,$9}'

#3
head receipt.csv -n11 | sed -r "1s/ymd/date/" | awk -F, '{print $1,$6,$7,$9}'

#4
cat receipt.csv | grep -e "CS018205000001" | awk -F, '{print $1,$6,$7,$9}'

#5
cat receipt.csv | sed -nr "/CS018205000001/p" | awk -F, '{print $1,$6,$7,$9}' | sed -nr "/^\w+?\W\w+?\W\w+?\W\w{4,}/p"

#6
cat receipt.csv | sed -nr "/CS018205000001/p" | awk -F, '{p=0;if($8>4) p=1; if($9>999) p=1; if(p==1) print $1,$6,$7,$8,$9}'

#7
cat receipt.csv | sed -nr "/CS018205000001/p" | awk -F, '{if($9<2001) if($9>999) print $1,$6,$7,$8,$9}'

#8
cat receipt.csv | sed -nr "/P071401019/d;/CS018205000001/p"

#9
cat store.csv | awk -F, '{if($3!=13) if($10<901) print}'

#10
cat store.csv | grep -e "^S14" | head -n10

#11
cat customer.csv | sed -rn "/^\w+?1,/p" | head -n10

#12
cat store.csv | grep "横浜市"

#13
cat customer.csv | sed -nr "/[A-F]-\w+?-\w$/p" | head -n10

#14
cat customer.csv | sed -nr "/\w-\w+?-[0-9]$/p" | head -n10

#15
cat customer.csv | sed -nr "/[A-F]-\w+?-[0-9]$/p" | head -n10

#16
cat store.csv | sed -nr "/\w{3}-\w{3}-\w{4}/p"

#17
sed 1d customer.csv | sort -t, -k5 | head -n10

#18
sed 1d customer.csv | sort -t, -k5r | head -n10

#19
sed 1d receipt.csv | sort -t, -k9rn | head -n10 | awk -F, 'BEGIN{r=0;p=20000} {if(p>$9) {r+=1;p=$9} print r,$6,$9}'

#20
sed 1d receipt.csv | sort -t, -k9rn | head -n10 | awk -F, '{print $6,$9}' | nl

#21
sed 1d receipt.csv | wc -l

#22
sed 1d receipt.csv | cut -d, -f6 | sort | uniq -c

#23
sed 1d receipt.csv | awk -F, '{q[$3]+=$8;a[$3]+=$9} END{for(s in a) {print s,a[s],q[s]}}'

#24
sed 1d receipt.csv | cut -d, -f1,6 | awk -F, '{if($2 in d) {if ($1>d[$2]) d[$2]=$1} else {d[$2]=$1}} END{for(i in d) {print i,d[i]}}' | head -n10

#25
sed 1d receipt.csv | cut -d, -f1,6 | awk -F, '{if($2 in d) {if ($1<d[$2]) d[$2]=$1} else {d[$2]=$1}} END{for(i in d) {print i,d[i]}}' | head -n10

#26
diff -y \
<(sed 1d receipt.csv | cut -d, -f1,6 | awk -F, '{if($2 in d) {if ($1>d[$2]) d[$2]=$1} else {d[$2]=$1}} END{for(i in d) {print i,d[i]}}' | sort -k1) \
<(sed 1d receipt.csv | cut -d, -f1,6 | awk -F, '{if($2 in d) {if ($1<d[$2]) d[$2]=$1} else {d[$2]=$1}} END{for(i in d) {print i,d[i]}}' | sort -k1) | \
grep "|" | head -n10

#27
sed 1d receipt.csv | awk -F, '{c[$3]+=1;a[$3]+=$9} END{for(s in a) {print s,a[s]/c[s]}}' | sort -k2rn | head -n5

#28
sed 1d receipt.csv | cut -d, -f3,9 | sort -t, -k1,1 -k2n | awk -F, '{if(s!=$1) {print s,a[int(c/2)];c=0;s=$1}} {a[c]=$2;c++} END{print s,a[int(c/2)]}' | sort -k2rn | head -n5

#29
sed 1d receipt.csv | cut -d, -f3,7 | sort | \
awk -F, '{if(s!=$1) {c=0;m="";for(p in a) {if(a[p]>c) {c=a[p];m=p}} print s,m;s=$1;delete a}} {a[$2]++} END{c=0;m="";for(p in a) {if(a[p]>c) {c=a[p];m=p}};print s,m}'

#30
sed 1d receipt.csv | cut -d, -f6,9 | awk -v avg="$(sed 1d receipt.csv | cut -d, -f6,9 | awk -F, '{sum+=$2} END{print sum/NR}')" -F, '{c[$1]++;d[$1]+=($2-avg)**2} END{for(s in d) {printf "%s %8.1f\n",s,d[s]/c[s]}}' | sort -k2rn | head -n5

#31
sed 1d receipt.csv | cut -d, -f6,9 | awk -v avg="$(sed 1d receipt.csv | cut -d, -f6,9 | awk -F, '{sum+=$2} END{print sum/NR}')" -F, '{c[$1]++;d[$1]+=($2-avg)**2} END{for(s in d) {printf "%s %8.1f\n",s,sqrt(d[s]/c[s])}}' | sort -k2rn | head -n5

#32
sed 1d receipt.csv | cut -d, -f9 | sort -k1n | awk '{a[NR]=$1} END{print int(NR/4),a[int(NR/4)];print int(NR/2),a[int(NR/2)];print int(3*NR/4),a[int(3*NR/4)];}'

#33
sed 1d receipt.csv | awk -F, '{c[$3]+=1;a[$3]+=$9} END{for(s in a) {if(a[s]/c[s]>330) {print s,a[s]/c[s]}}}'

#34
sed "1d;/ZZ/d" receipt.csv | awk -F, '{sum+=$9} END{print sum/NR}'

#35
sed '1d;/ZZ/d' receipt.csv | cut -d, -f6,9 | \
awk -v avg="$(sed '1d;/ZZ/d' receipt.csv | awk -F, '{sum+=$9} END{print sum/NR}')" -F, '{c[$1]++;a[$1]+=$2} END{for(s in a) {if(a[s]/c[s]>avg) {print s,a[s]/c[s]}}}' | head -n10

#36
join -t, -1 3 \
<(sed 1d receipt.csv | sort -t, -k3) \
<(sed 1d store.csv | cut -d, -f1,2 | sort -t, -k1) | head -n10

#37
join -t, -1 4 \
<(sed 1d product.csv | sort -t, -k4) \
<(sed 1d category.csv | cut -d, -f5,6 | sort -t, -k1) | head -n10

#38
join -a1 -o auto -e 0 \
<(cat customer.csv | grep "女性" | cut -d, -f1 | sort -k1) \
<(sed '1d;/ZZ/d' receipt.csv | cut -d, -f6,9 | awk -F, '{a[$1]+=$2} END{for(s in a) {print s,a[s]}}' | sort -k1) | head -n10

#39
join -j1 -a1 -a2 -o auto -e "-" \
<(sed "1d;/ZZ/d" receipt.csv | cut -d, -f1,6 | sort -t, -k2 | uniq | cut -d, -f2 | uniq -c | sort -k1rn | awk '{print $2,$1}' | head -20 | sort -k1) \
<(sed "1d;/ZZ/d" receipt.csv | cut -d, -f6,9 | awk -F, '{c[$1]++;a[$1]+=$2} END{for(s in a) {print s,a[s]/c[s]}}' | sort -k2rn | head -n20 | sort -k1)

#40
echo $((`sed 1d store.csv | wc -l`*`sed 1d product.csv | wc -l`))

#41
sed 1d receipt.csv | cut -d, -f1,9 | sort -k1 | awk -F, '{a[$1]+=$2} END{b=0;for(s in a) {print s,a[s]-b;b=a[s]}}' | head -n10

#42
sed 1d receipt.csv | cut -d, -f1,9 | sort -k1 | awk -F, '{a[$1]+=$2} END{b1=0;b2=0;b3=0;for(s in a) {print s,a[s],b1,b2,b3;b3=b2;b2=b1;b1=a[s]}}' | head -n10

#43
join -t, \
<(sed '1d;/ZZ/d' receipt.csv | cut -d, -f6,9 | sort -k1) \
<(sed 1d customer.csv | cut -d, -f1,3,6 | sort -k1) | \
awk -F, '{a[$3][int($4/10)]+=$2} END{for(g in a[1]) {printf "\t%s", g}{printf "\n"}; for(s in a) {printf "%s",s; for(g in a[s]) {printf "\t%d",a[s][g]} {printf "\n"}}}'

#44
join -t, \
<(sed '1d;/ZZ/d' receipt.csv | cut -d, -f6,9 | sort -k1) \
<(sed 1d customer.csv | cut -d, -f1,3,6 | sort -k1) | \
awk -F, '{a[$3][int($4/10)]+=$2} END{for(s in a) {for(g in a[s]) {if(s<2) {s2="0"s} else {s2=99}; print g"0代",s2,a[s][g]}}}'

#45
sed 1d customer.csv | cut -d, -f1,5 | sed -r "s/-//g" | head -n10

#46
sed 1d customer.csv | cut -d, -f1,10 | awk -F, '{print $1,substr($2,0,4)"-"substr($2,5,2)"-"substr($2,7,2)}' | head -n10

#47
sed 1d receipt.csv | cut -d, -f1,4,5 | awk -F, '{print substr($1,0,4)"-"substr($1,5,2)"-"substr($1,7,2),$2,$3}' | head -n10

#48
sed 1d receipt.csv | cut -d, -f2,4,5 | awk -F, '{print strftime("%Y%m%d", $1), $2, $3}' | head -n10

#49
sed 1d receipt.csv | cut -d, -f2,4,5 | awk -F, '{print strftime("%Y", $1), $2, $3}' | head -n10

#50
sed 1d receipt.csv | cut -d, -f2,4,5 | awk -F, '{print strftime("%m", $1), $2, $3}' | head -n10

#51
sed 1d receipt.csv | cut -d, -f2,4,5 | awk -F, '{print strftime("%d", $1), $2, $3}' | head -n10

#52


#53


#54


#55


#56


#57


#58


#59


#60


#61


#62


#63


#64


#65


#66


#67


#68


#69


#70


#71


#72


#73


#74


#75


#76


#77


#78


#79


#80


#81


#82


#83


#84


#85


#86


#87


#88


#89


#90


#91


#92


#93


#94


#95


#96


#97


#98


#99


#100


